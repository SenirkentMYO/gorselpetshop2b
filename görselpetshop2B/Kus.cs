//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace görselpetshop2B
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kus
    {
        public int ID { get; set; }
        public string KusAd { get; set; }
        public Nullable<int> KusTipiID { get; set; }
        public Nullable<int> KusCinsID { get; set; }
        public Nullable<int> KusRenkID { get; set; }
        public string KusYasi { get; set; }
        public Nullable<int> KusOmru { get; set; }
        public Nullable<int> KusFiyat { get; set; }
        public Nullable<int> KusStok { get; set; }
    }
}
