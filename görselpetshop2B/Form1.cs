﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace görselpetshop2B
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PetshopDBEntities database = new PetshopDBEntities();
            List<Kullanici> HayvanListesi = database.Kullanici.Where(Kullanici => Kullanici.Email == textBox1.Text && Kullanici.Sifre == textBox2.Text).ToList();
            if (HayvanListesi.Count > 0)
            {
                this.Hide();

                Form4 b = new Form4();
                b.ShowDialog();

            }
            else
            {
                MessageBox.Show("giriş başarısız");


            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
